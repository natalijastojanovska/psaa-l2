package com.company;
import com.company.Main;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class CharStack {
    private LinkedList<Character> items;

    public CharStack(){
        this.items = new LinkedList<Character>(); //current class constructor
    }
    public void push(Character item) {
        items.push(item);
    }

    public Character peek() {
        return items.getFirst();
    }

    public Character pop() {
        Iterator<Character> iter = items.iterator(); //iterator loops through collections
        Character item = iter.next ();
        if(item != null){
            iter.remove();
            return item;
        }
        return null;
    }

    public boolean empty(){
       return (items.isEmpty());
    }
}

