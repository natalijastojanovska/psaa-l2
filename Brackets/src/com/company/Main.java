package com.company;
import com.company.CharStack;
import java.util.Scanner;
import com.company.CharStack;
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter mathematical expression: ");
        String mathExp = scan.nextLine();

        balancedBrackets(mathExp);
    }
    static void balancedBrackets(String mathExp){
        CharStack brackets = new CharStack();
        tuka: for (int i=0; i<mathExp.length(); i++){
            char c = mathExp.charAt(i);
            switch (c){
                case '(':
                    brackets.push(c);
                    break;
                case '{':
                    brackets.push(c);
                    break;
                case '[':
                    brackets.push(c);
                    break;
                case ')':
                    if('(' == brackets.peek()) {
                        brackets.pop();
                        break;
                    }
                    else{
                        break tuka;
                    }
                case '}':
                    if('{' == brackets.peek()) {
                        brackets.pop();
                        break;
                    }
                    else{
                        break tuka;
                    }
                case ']':
                    if('[' == brackets.peek()) {
                        brackets.pop();
                        break;
                    }
                    else{
                        break tuka;
                    }
            }
        }
        if (brackets.empty()){
            System.out.println("Balanced Brackets. ");
        }
        else {
            System.out.println("Unbalanced Brackets. ");
        }
    }
}
