package com.company;

import java.util.Iterator;

public class DLinkedList<Integer> {
    class Node{
        int data;
        Node previous;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    Node head, tail = null;

    public void insertLast(int data) {
        Node newNode = new Node(data);

        if(head == null) {
            head = tail = newNode;
            head.previous = null;
            tail.next = null;
        }
        else {
            tail.next = newNode;
            newNode.previous = tail;
            tail = newNode;
            tail.next = null;
        }
    }

    public void display() {
        Node current = head;
        if(head == null) {
            System.out.println("List is empty");
            return;
        }
        System.out.println("Even nodes of doubly linked list: ");
        while(current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
    }
    void deleteNode(Node del) {

        if (head == null || del == null) {
            return;
        }

        if (head == del) {
            head = del.next;
        }

        if (del.next != null) {
            del.next.previous = del.previous;
        }

        if (del.previous != null) {
            del.previous.next = del.next;
        }

        return;
    }
    public int size(){
        Node temp = head;
        int b = 0;
        while(temp != null){
            b++;
            temp=temp.next;
        }
        return b;
    }
    public int get(int i){
        Node temp = head;
        for (int j=0; j!=i; j++){
            temp=temp.next;
        }
        return temp.data;
    }
}
