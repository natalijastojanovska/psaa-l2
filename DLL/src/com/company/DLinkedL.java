package com.company;
import com.company.DLinkedList;


public class DLinkedL <DLinkedList> {
    class Node{
        DLinkedList data;
        Node previous;
        Node next;

        public Node(DLinkedList data) {
            this.data = data;
        }
    }

    Node head, tail = null;

    public void insertLast(DLinkedList data) {
        Node newNode = new Node(data);

        if(head == null) {
            head = tail = newNode;
            head.previous = null;
            tail.next = null;
        }
        else {
            tail.next = newNode;
            newNode.previous = tail;
            tail = newNode;
            tail.next = null;
        }
    }

    public DLinkedList get(int i){
        Node temp = head;
        for (int j=0; j!=i; j++){
            temp=temp.next;
        }
        return temp.data;
    }

    public int size(){
        Node temp = head;
        int b = 0;
        while(temp != null){
            b++;
            temp=temp.next;
        }
        return b;
    }


}
