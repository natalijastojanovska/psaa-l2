package com.company;
import com.company.DLinkedList;
import com.company.DLinkedL;
import java.util.Scanner;

public class DLinkedListOfDLinkedList {
    public static DLinkedList<Integer> formirajNova(DLinkedL list) {
        DLinkedList<Integer> nova = new DLinkedList();
tuka:   for(int i = 0; i < list.size(); i++){
            DLinkedList<Integer> temp = (DLinkedList<Integer>) list.get(i);
            for(int k = 0; k < temp.size(); k++){
                if(temp.get(k) % 2 != 0){
                    continue tuka;
                }
            }
            for(int k = 0; k < temp.size(); k++){
                nova.insertLast(temp.get(k));
            }
        }
        return nova;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter number of sublists: ");
        int subLists = input.nextInt();
        System.out.println("Enter number of elements: ");
        int elements = input.nextInt();
        System.out.println("Enter the elements of ");
        DLinkedL list = new DLinkedL();
        for (int i = 0; i < subLists; i++) {
            System.out.println( (i+1) + " sublist: ");
            DLinkedList<Integer> tmp = new DLinkedList<Integer>();
            for (int j = 0; j < elements; j++) {
                tmp.insertLast(input.nextInt());
            }
            list.insertLast(tmp);
        }
        DLinkedList<Integer> nova = formirajNova(list);
        nova.display();
    }

}
